#!/usr/bin/python2
import sys, os
sys.path.append(os.path.join(os.path.dirname(__file__), '../tools/transliterate'))
import trans
import codecs

alpha = 'abcdefghijklmnopqrstuvwxyz'
alpha +=  alpha.upper()
alpha += '"()_0123456789 '

def get_hk_transliteration(data):
    hk_data = trans.transliterate(data, trans.DEVANAGARI, trans.HK)
    return ''.join([c for c in hk_data if c in alpha])

if __name__ == '__main__':
    if len(sys.argv) != 3:
        print "ERROR! Not enough arguments."
        print "Arg1: path to indic language file."
        print "Arg2: path to Harvard Kyoto format output file."
        sys.exit(0)

    with codecs.open(sys.argv[1], 'r', encoding='utf-8') as f:
        lines = f.readlines()

    hk_lines = [get_hk_transliteration(line) for line in lines]

    with open(sys.argv[2], 'w') as f:
        for line in hk_lines:
            f.write(line + '\n')

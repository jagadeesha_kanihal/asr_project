#!/bin/bash

function help {
    echo 'USAGE: to_kaldi_output.sh <file with text transcriptions> <wav directory> <output_directory>'
    echo 'NOTE: All paths must be relative to project root directory.'
}

if [ $# -eq 3 ] 
then
    echo "starting"
else
    echo "invalid argument please pass only one argument "
    help
    exit 1
fi

# SCRIPT_DIR="$(dirname "${BASH_SOURCE[0]}" )"
# cd $SCRIPT_DIR/../

if [[ -d "$3" ]]
then
	echo "removing old directory"
    rm -rf "$3"
    # help
    # exit 1
fi

mkdir "$3"

cut -d ' ' -f 2 "$1" > "$3"/tmp1
cut -d '"' -f 2 "$1" | cut -d ' ' -f 2- > "$3"/tmp2
paste -d ' ' "$3"/tmp1 "$3"/tmp2 > "$3"/text
awk '{print $1 " spk_1"}' < "$3"/tmp1 > "$3"/utt2spk
awk -v path="$2" '{print $1 " " path "/" $1 ".wav"}' < "$3"/tmp1 > "$3"/wav.scp

rm "$3/tmp1" "$3/tmp2"

echo "Fixing"
./scripts/fix_data_dir.sh "$3"
echo "Done"
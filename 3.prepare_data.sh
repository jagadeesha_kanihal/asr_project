#!/bin/bash
./scripts/to_kaldi_format.sh "./data/both/train.txt" "./data/both/wav" "./data/kaldi-format/train"
./scripts/to_kaldi_format.sh "./data/both/test.txt" "./data/both/wav" "./data/kaldi-format/test"
echo "------asr dataprep-------"
python2 ./scripts/asr_dataprep.py
echo "--------Done----------"

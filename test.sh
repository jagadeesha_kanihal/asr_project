#!/bin/bash

function help {
    echo 'USAGE: train.sh [OPTIONS]'
    echo '--expdir          Output directory'
    echo '--neural-net      Neural net config'
    echo '--trainer         Trainer config'
    echo '--decoder         Decoder config'
}

SHORT=h
LONG=expdir:,neural-net:,trainer:,decoder:

OPTIONS=$(getopt --options "$SHORT" --longoptions "$LONG" --name "$0" -- "$@")

if [[ $? -ne 0 ]]
then
    help
    exit 1
fi

eval set -- "$OPTIONS"

EXPDIR='experiment/run'
NNET='config/nabu/LAS.cfg'
TRAINER='config/nabu/cross_entropytrainer.cfg'
DECODER='config/nabu/BeamSearchDecoder.cfg'


while true
do
    case "$1" in
        --expdir)
            EXPDIR="$2"
            shift 2
            ;;
        --neural-net)
            NNET="$2"
            shift 2
            ;;
        --trainer)
            TRAINER="$2"
            shift 2
            ;;
        --decoder)
            DECODER="$2"
            shift 2
            ;;
        -h)
            help
            exit 1
            ;;
        --)
            shift
            break
            ;;
        *)
            help
            exit 1
            ;;
    esac
done

#./scripts/run_train.py --expdir="$EXPDIR" --type=asr --network="$NNET" --trainer="$TRAINER" --decoder="$DECODER"
./scripts/test_asr.py --expdir="$EXPDIR" #--type=asr --network="$NNET" --trainer="$TRAINER" --decoder="$DECODER"

"""NOTE: This is automatically copied to the nabu source directory"""
import normalizer

class HarvardKyoto(normalizer.Normalizer):

    def __call__(self, transcription):

        transcription = ' '.join([word for word in transcription.split()])
        transcription = list(transcription)
        normalized = [c if c in self.alphabet else '<space>' if c is ' ' else '<unk>' \
                            for c in transcription]

        return ' '.join(normalized)

    def _create_alphabet(self):

        alphabet = []
        alphabet.append('<space>')
        alphabet.append('<unk>')

        chars = 'abcdefghijklmnoprstuvxyz'
        chars += 'ADGHIJMNRSTU'

        alphabet += list(chars)

        return alphabet

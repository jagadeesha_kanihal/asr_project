'''@file normalizer_factory.py
Contains the normalizer factory

NOTE: This will be automatically copied to nabu source directory
'''

import aurora4
import timit
import harvard_kyoto_normalizer

def factory(normalizer_type):
    '''create a normalizer_type

    Args:
        normalizer_type: the type of normalizer_type

    Returns:
        a normalizer function'''

    if normalizer_type == 'aurora4_normalizer':
        return aurora4.Aurora4()
    elif normalizer_type == 'timit_phone_norm':
        return timit.Timit()
    elif normalizer_type == 'hk':
        return harvard_kyoto_normalizer.HarvardKyoto()
    else:
        raise Exception('Undefined normalizer: %s' % normalizer_type)
